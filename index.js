const yargs = require("yargs");
const { simpanContact, listContact, detailContact, hapusContact } = require("./app.js");

yargs.command({
	command: "add",
	describe: "Menambahkan data",
	builder: {
		nama: {
			describe: "Nama Lengkap",
			demandOption: true,
			type: "string"
		},
		email: {
			describe: "Email",
			demandOption: true,
			type: "string"
		},
		nomor: {
			describe: "Nomor",
			demandOption: false,
			type: "string"
		}
	},
	handler(argv) {
		simpanContact(argv.nama, argv.email, argv.nomor);
	}
})
.demandCommand();

yargs.command({
	command: "list",
	describe: "List data",
	handler(argv) {
		listContact();
	}
});

yargs.command({
	command: "detail",
	describe: "Detail nama",
	builder: {
		nama: {
			describe: "Nama detail",
			demandOption: true,
			type: "string"
		}
	},
	handler(argv) {
		detailContact(argv.nama);
	}
});

yargs.command({
	command: "delete",
	describe: "Delete contact",
	builder: {
		nama: {
			describe: "Hapus berdasarkan nama",
			demandOption: true,
			type: "string"
		}
	},
	handler(argv) {
		hapusContact(argv.nama);
	}
});

yargs.parse();
