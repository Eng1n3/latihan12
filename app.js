const fs = require("fs");
const chalk = require("chalk");
const validator = require("validator");

const pathDir = "./data/";
if (!fs.existsSync(pathDir)) fs.mkdirSync(pathDir);

const dataDB = "./data/db.json";
if (!fs.existsSync(dataDB)) fs.writeFileSync(dataDB, "[]", "utf8");

const loadContact = () => {	
	const file = fs.readFileSync(dataDB, "utf-8");
	return JSON.parse(file);
}

const simpanContact = (nama, email, nomor) => {
	const contact = { nama, email, nomor };
	const contacts = loadContact();
	const duplikatNama = contacts.find(contact => contact.nama === nama);
	if (duplikatNama) {
		console.log(chalk.red.inverse.bold("Nama sudah digunakan"));
		return false;
	};
	if (email) {
		if(!validator.isEmail(email)) {
			console.log(chalk.red.inverse.bold("Email tidak valid"));
			return false;
		}
	}
	if (nomor) {
		if(!validator.isMobilePhone(nomor, "id-ID")) {
			console.log(chalk.red.inverse.bold("Nomor tidak valid"));
			return false;
		}
	}
	contacts.push(contact);
	fs.writeFileSync(dataDB, JSON.stringify(contacts), "utf8");
}

const listContact = () => {
	const contacts = loadContact();
	console.log(chalk.green.inverse.bold("Daftar nama: "))
	contacts.forEach((contact, i) => {
		console.log(`${i + 1}. ${contact.nama} - ${contact.email} ${contact.nomor? ` - ${contact.nomor}`: ""}`);
	})
};

const detailContact = nama => {
	const contacts = loadContact();
	const namaContact = contacts.find(contact => contact.nama.toLowerCase() === nama.toLowerCase());
	if (!namaContact) {
		console.log(chalk.red.inverse.bold("Nama tidak ada"));
		return false;
	}
	console.log(`${namaContact.nama} - ${namaContact.email}${namaContact.nomor? ` - ${namaContact.nomor}` : ""}`)
}

const hapusContact = nama => {
	const contacts = loadContact();
	const newContacts = contacts.filter(contact => contact.nama.toLowerCase() !== nama.toLowerCase());
	if (newContacts.length === contacts.length) {
		console.log(chalk.red.inverse.bold("Nama tidak ditemukan"));
		return false;
	}
	fs.writeFileSync(dataDB, JSON.stringify(newContacts), "utf8");
	console.log(chalk.green.inverse.bold(`${nama} berhasil dihapus`));
}

module.exports = { simpanContact, listContact, detailContact, hapusContact };
